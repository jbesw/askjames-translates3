# askJames-translateS3

A Lambda function that responds to S3 Put events to translate text files into other languages using AWS Translate. This is demo code from a Stackery webinar on 2/27/19.

- The index-serial.js file shows how requests are queued and only occur once the previous operation is completed. Execution length is around 8 seconds for this version.
- The index-parallel.js file shows how requests occur concurrently. Execution length is around 3 seconds for this version.

In both cases, the function is responding to an S3 event and will write the new translations in a 'translation folder' with a language prefix on the file name.

# Prerequisites

The function requires an S3 bucket and linking to the put event for files with a .txt suffix. 
The IAM role requires access to AWS Translate and read/write to the S3 bucket.

# Questions

Please contact James Beswick @jbesw on Twitter. Feel free to use this code however you choose, though no warranty is implied.