const AWS = require('aws-sdk')
AWS.config.region = 'us-east-1'
const translate = new AWS.Translate()

const translateText = async (originalText, targetLanguageCode) => {
    return new Promise((resolve, reject) => {
        let params = {
            Text: originalText,
            SourceLanguageCode: "auto",
            TargetLanguageCode: targetLanguageCode
        }
    
        try {
            translate.translateText(params, (err, data) => {
                if (err) {
                    console.log('Error: ', err)
                    reject(err)
                }

                console.log('Data: ', data)
                if (data) resolve(data)
            })
        } catch (err) {
            console.error(err)
        }
    })
}

module.exports = { translateText }