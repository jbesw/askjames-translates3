const AWS = require('aws-sdk')
AWS.config.region = 'us-east-1'

const { getS3object, putS3object }  = require('./s3')
const { translateText } = require('./translate')

// The standard Lambda handler

// Entire list of language codes at: https://docs.aws.amazon.com/translate/latest/dg/API_TranslateText.html
const targetLanguages = ['fr', 'de', 'it', 'es', 'sv', 'ar', 'pt', 'ru']

exports.handler = async (event) => {

    // Don't fire for any new file in the translations folder
    if (event.Records[0].s3.object.key.indexOf('translations') > -1) return

    console.time('test')
    
    await Promise.all(
        targetLanguages.map(async (targetLanguage) => {
            await doTranslation(event, targetLanguage)
        })
    )

    console.timeEnd('test')

    // We're done!
    return {
        statusCode: 200,
        body: JSON.stringify('OK'),
    }
}

// The translation function

const doTranslation = async (event, targetLanguage) => {
    return new Promise(async (resolve, reject) => {
        
        // Get original text from object in incoming event
        const originalText = await getS3object({
            Bucket: event.Records[0].s3.bucket.name,
            Key: event.Records[0].s3.object.key
        })
    
        // Translate the text
        const data = await translateText(originalText.Body.toString('utf-8'), targetLanguage)

        // Save the new translation
        const baseObjectName = event.Records[0].s3.object.key.replace('.txt','')
        await putS3object({
            Bucket: event.Records[0].s3.bucket.name,
            Key: `translations/${baseObjectName}-${targetLanguage}.txt`,
            Body: data.TranslatedText,
            ContentType: 'text/plain',
            ACL: 'public-read'
        })
        resolve()
    })
}